La résolution du problème de commande optimale nous donne une stratégie de commande,
i.e., une fonction $\hat u:\R\times\R^n \to \R^m$.
$\hat u(t, x)$ est la commande à choisir en fonction du temps et de l'état du système.\\
Ces méthodes peuvent être adaptées pour obtenir une stratégie indépendante du temps, en
utilisant une commande optimale avec un horizon glissant, par exemple.

Cela permet de considérer le système dynamique avec commande comme un système sans
commande, ayant la fonction de dynamique
\[
    x_{i+1} = \hat f(x_i) = f(x_i, \hat u(x_i)).
\]
Il est naturel de s'intéresser à l'étude des propriétés de ce système (stabilité du
système, points fixes, cycles limites, etc.).

La théorie de \textit{l'opérateur de Koopman} fournit un cadre pour faire cette étude, notamment
dans les cas où la fonction de dynamique du système n'est pas connue.

\section{Définition}%
\label{sec:definition}

On considère un système dynamique sur l'espace $\R^n$, avec une fonction de transition
$f$.\\
Soit $\F$ un espace vectoriel de fonctions\footnote{On considère typiquement les
fonctions continues.} de $\R^n$ vers $\R$, qu'on appelle des \textit{observables}.\\
L'opérateur de Koopman associé au système est un opérateur linéaire $\K: \F\to \F$ tel
que
\[
    \K(g)(x) = g(f(x)).
\]
Étant donnés une trajectoire $(x_i)_{i \in \mathbb N}$, cela permet d'avoir
\[
    \K(g)(x_i) = g(x_{i+1}).
\]
L'opérateur de Koopman regarde le système comme agissant sur les observables, au lieu des
états.
Il permet de représenter la dynamique par un système linéaire, mais sur un espace de
dimension infinie.\\
L'étude du spectre de cet opérateur peut donner des informations importantes sur les
propriétés qualitatives du système.\cite{mezic2017koopman}


\section{Ensembles attracteurs et stabilité}%
\label{sec:ensembles_attracteurs_et_stabilite}

On appelle système attracteur tout ensemble $A$ vérifiant:
\begin{enumerate}
    \item $f(A) \subset A$,
    \item Il existe un voisinage $V$ (appelé aussi bassin d'attraction) de $A$ tel que
        $\forall x \in V$, les points limites de la trajectoire commençant en $x$ sont
        dans $A$,
    \item Il n'existe aucun sous ensemble strict de $A$ et fermé qui satisfait les deux
        propriétés précédentes.
\end{enumerate}
Si de plus le bassin d'attraction est tout l'espace, on dit que l'attracteur est
globalement asymptotiquement stable.

Les attracteurs du système dynamique décrivent l'évolution asymptotique du système
dynamique, et sont d'un grand intérêt en pratique.
L'opérateur de Koopman permet de les étudier à travers son spectre.
En particulier, si $\phi$ est une fonction propre de $\K$, à valeur propre $\lambda$ tel
que $\mathfrak{Re}(\lambda) < 0$, alors l'ensemble des zéros de $\phi$ est un attracteur
globalement asymptotiquement stable.

L'étude asymptotique du système se réduit alors à la décomposition en valeurs propres
de $\K$, et s'applique à une grande classe de systèmes.
D'autre part, ces méthodes ne peuvent pas êtres appliquées au systèmes chaotiques, ou
dont le spectre de $\K$ est continu.\cite{Arbabi2018IntroductionTK}

L'étude analytique de l'opérateur de Koopman est impossible lorsque la fonction de
la dynamique n'est pas connue.
En pratique, on utilise des méthodes comme \textit{Dynamic Mode Decomposition},\cite{DMD} qui
consiste à considérer une approximation finie de l'opérateur de Koopman, par exemple en
considérant un sous espace de dimension fini de fonctions $F$, et en étudiant les valeurs
propres (en particulier, celles à partie réelle négative) de sa restriction sur ce sous
espace, projetée sur $F$.
\[
    \forall f \in F \K_F(f) = \mathcal P(K(f)).
\]
On espère que pour une classe assez large de fonctions, les propriétés de cette
paramétrisation peuvent donner une bonne idée sur les aspects du système dynamique.
