En pratique, la fonction de transition d'un système dynamique n'est pas toujours
disponible, et les modèles physiques classiques ne sont pas assez proches de la réalité
pour contrôler l'évolution du système.\\
Une solution possible est d'estimer la dynamique du système à partir de données réelles.\\
Considérons un jeu de données i.i.d.\ ${(X_i, Y_i)}_{1 \leq i \leq N}$, avec
$Y_i=X_i + f(X_i) + \epsilon_i$.\\
On propose plusieurs méthodes pour l'estimation de la fonction de transition

\section{Moindres carrés}%
\label{sec:least_squares}

On définit un vecteur d'observables $\Psi: \R^n\times\R^m \to \R^M$ et on prend comme
modèle de la dynamique
\[
    \hat f(x, u) = x + \hat \alpha\T \Psi(x, u) = x + \sum_{j = 1}^M \hat \alpha_j
    \psi_j(x).
\]
On choisit $\hat \alpha \in \R^{M\times n}$ pour minimiser l'erreur quadratique moyenne.
\begin{equation}
    \label{lstsq}
    \hat \alpha = \argmin_\alpha \sum_{i=1}^N \lVert Y_i - X_i - \alpha\T \Psi(X_i)
    \rVert^2.
\end{equation}
La solution de ce problème de minimisation est donnée par
\[
    \hat\alpha = G^\dagger A,
\]
où $G^\dagger$ est la pseudoinverse de Moore-Penrose de $G$ et
\begin{align*}
    G &= \sum_{i=1}^N \Psi(X_i)\T \Psi(X_i),\\ A &= \sum_{i=1}^N \Psi(X_i)\T Y_i.
\end{align*}
Ce modèle peut ensuite être utilisé directement dans l'algorithme de programmation
différentielle dynamique pour obtenir la commande optimale pour minimiser une certaine
fonction coût.

Cette méthode exige une sélection des observables $\Psi$, qui peut avoir un grand effet
sur la qualité de l'estimateur.
Il est possible d'obtenir des garanties de convergence en considérant une base de
fonctions ${(\phi_i)}_{i \in \mathbb N}$ sur $\R^n \times \R^m$, et en prenant
\begin{equation*}
    \Psi_M = \begin{pmatrix}
        \phi_1 \\ \vdots \\ \phi_M
    \end{pmatrix}.
\end{equation*}
Mais le choix d'une base mal adaptée au problème peut aboutir à une convergence trop
lente.

\section{Estimation par noyau}%
\label{sec:kernel_methods}
Soit $K:\R^{n+m} \times \R^{n+m} \to \R$ un noyau.\\
On suppose qu'il s'agit d'un noyau stationnaire, i.e., il existe $k: \R^{n+m} \to \R$ tel
que
\begin{equation*}
    \forall x, y \in \R^{n+m},\quad K(x, y) = k(x - y),
\end{equation*}
On s'intéresse aux estimateurs qui s'écrivent sous la forme:
\begin{equation*}
    \hat f(x) = \sum_{i=1}^N \alpha_i K(x, X_i),
\end{equation*}
où les coefficients $\alpha_i \in \R^n$ sont déterminés par régression linéaire
régularisée.\\
Souvent en pratique, on considère une famille paramétrique de noyaux, dont les
hyperparamètres (y inclus les facteurs de régularisation) sont déterminés à partir des
données.

Une manière pour sélectionner les hyperparamètres consiste à séparer le jeu de données en
un ensemble d'entraînement et un ensemble de validation.
On choisit ensuite les hyperparamètres tels que l'erreur de validation de l'estimateur,
entrainé sur l'ensemble d'entraînement, soit minimale.

Classiquement, cela est fait par un grid search ou une recherche aléatoire, mais ces
méthodes ne sont efficaces que lorsque pour un nombre faible d'hyperparamètres, car le
temps de calcul croît exponentiellement avec la dimension pour une grille à précision
fixée.\\
Si les hyperparamètres sont continus, il est également possible d'utiliser des outils
d'optimisation différentiable, qui sont moins sensible à la dimension du problème, pour
déterminer les hyperparamètres optimaux.\\
Pour exploiter cette structure, il suffit de calculer les dérivées premières (et secondes
si on souhaite accélérer la convergence) de l'erreur de validation par rapport aux
hyperparamètres.

Les familles de noyaux utilisés seront discutés plus en détail par la suite dans le
rapport.

Comme pour l'estimateur à moindres carrés dans la section précédente, les dérivées de $f$
sont estimées par les dérivées de $\hat f$ pour l'itération de la PDD\@.
