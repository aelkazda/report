La programmation différentielle dynamique\cite{ddp} est un algorithme itératif permettant
d'améliorer une suite de commandes, en utilisant les dérivées secondes des fonctions
du problème.\\
La commande donnée par l'algorithme est similaire à la commande obtenue par un pas de
Newton sur le problème d'optimisation, et permet donc d'avoir une convergence
quadratique, avec moins de calcul.
En particulier, pour un système dynamique où la fonction de transition $f$ est linéaire
et les coûts partiels $\ell_i, \ell_f$ sont quadratiques, la PDD permet d'obtenir la
commande optimale en une seule itération.\\
La complexité numérique des factorisations de matrices dans l'itération de PDD est
$O(Nm^3)$, alors que la factorisation de la Hessienne de la fonction coût pour déterminer
le pas de Newton a une complexité de $O(N^3 m^3)$.

Une itération de programmation différentielle dynamique se fait en deux étapes:
\textit{un parcours inverse} pour calculer une solution approximée de~\eqref{value} basée
sur des approximations du second ordre, suivi par un parcours direct pour calculer une
suite de commandes améliorée.

\subsection{Approximation quadratique et parcours inverse}%
\label{sub:quadratic_approximation}
On se donne une trajectoire $(\x, \U)$.\\
Soit
\begin{equation}
    Q_i(x, u) = \ell_i(x, u) + V_{i+1}(f(x, u)). \label{q_recursive}
\end{equation}
On a par définition
\begin{equation}
    V_i(x) = \min_u Q(x, u). \label{v_recursive}
\end{equation}
On approxime $Q_i$ et $V_i$ par des modèles quadratiques:
\begin{align*}
    Q_i(x, u) &\approx \tfrac12 \delta x\T Q_{i}^{xx} \delta x + \delta u\T Q_{i}^{ux} \delta
    x + \tfrac12 \delta u\T Q_{i}^{uu} \delta u + q_{i}^{x} \delta x + q_{i}^{u} \delta u +
    q_i,\\
    V_i(x) &\approx \tfrac12 \delta x\T V_{i}^{xx} \delta x + v_{i}^{x} \delta x + v_i,
\end{align*}
où $\delta x$ (resp.\ $\delta u$) dénote $x-x_i$ (resp.\ $u-u_i$).

En imposant les conditions~\eqref{q_recursive} et~\eqref{v_recursive}, ainsi que la
définition de $V_N$, on peut déterminer les valeurs de chaque terme du modèle par
récurrence.
\begin{align*}
    v_N  &= \ell_f,\\
    v_N^x  &= \ell_f^x,\\
    V_N^{xx} &= \ell_f^{xx}.
\end{align*}
Et pour $0 \leq i \leq N-1$,
\begin{subequations}\label{q_model_recursive}
    \begin{align}
        q_i      &= \ell_i + v_{i+1},\\
        q_{i}^{x}  &= \ell_i^{x\TT} + f^{x\TT} v_{i+1}^{x},\\
        q_{i}^{u}  &= \ell_i^{u\TT} + f^{u\TT} v_{i+1}^{x},\\
        Q_{i}^{xx} &= \ell_i^{xx} + f^{x\TT} V_{i+1}^{xx}f^x + V_{i+1}^{x}\cdot f^{xx},\\
        Q_{i}^{ux} &= \ell_i^{ux} + f^{x\TT} V_{i+1}^{xx}f^x + V_{i+1}^{x}\cdot f^{ux},\\
        Q_{i}^{uu} &= \ell_i^{uu} + f^{x\TT} V_{i+1}^{xx}f^x + V_{i+1}^{x}\cdot f^{uu},
    \end{align}
\end{subequations}
où $\cdot$ est un produit tensoriel.

On construit le modèle de $V_i$ en minimisant cette expression par rapport à $u$.
Le minimum est atteint en $k + K\delta x$, avec
\begin{subequations}\label{quad_solve}
    \begin{align}
        k_i &= -{(Q_{i}^{uu})}^{-1} {q_{i}^{u}}\T, \label{ff_solve}\\
        K_i &= -{(Q_{i}^{uu})}^{-1} Q_{i}^{ux}.\label{fb_solve}
    \end{align}
\end{subequations}
On appelle $k_i$ le \textit{feedforward} de la commande, et $K_i$ le \textit{feedback}.\\
On déduit alors
\begin{align*}
    v_i      &= q_i - \tfrac12 k_i\T Q_{i}^{uu} k_i,\\
    v_{i}^{x}  &= q_{i}^{x} - k_i\T Q_{i}^{uu} K_i,\\
    V_{i}^{xx} &= q_{i}^{xx} - K_i\T Q_{i}^{uu} K_i.
\end{align*}

\subsection{Parcours direct}%
\label{sub:forward_pass}

Après avoir calculé la suite de feedforwards et feedbacks, on calcule la commande
améliorée récursivement:
\begin{subequations}
    \begin{align}
        \hat x_0 &= x_0, \\
        \hat u_i &= u_i + \alpha k_i + K_i (\hat x_i - x_i), \label{fwd_pass}\\
        \hat x_i &= f(\hat x_i, \hat u_i),
    \end{align}
\end{subequations}
où le pas $\alpha \in \interval[open right]{0}{1}$ est un paramètre de la recherche
linéaire, qu'on choisit de manière adaptative.

\subsection{Direction de recherche asymptotique}%
\label{sub:asymptotic_search_direction}

Quand le pas $\alpha$ tend vers $0$, la commande obtenue $\hat \U$ est asymptotiquement
\[
    \hat \U = \U + \alpha d\hat \U + o(\alpha),
\]
où $d\hat \U = (d\hat u_0, d\hat u_1, \dots, d\hat u_{N-1})$ est définie par
\begin{align*}
    d\hat x_0 &= 0,\\
    d\hat u_i &= k_i + K_i d\hat x_i,\\
    d\hat x_{i+1} &= f^x(x_i, u_i) d\hat x_i + f^u(x_i,
    u_i)d\hat{u}_i.
\end{align*}

\subsection{Considérations pratiques}%
\label{sub:considerations_pratiques}

Pour construire le modèle de $V_i$, on suppose que celui de $Q_i$ peut être minimisé par
rapport à $u$, i.e., que la matrice $Q_i^{uu}$ est définie positive.\\
En dehors de quelques cas dégénérés, cette condition est toujours satisfaite dès que
notre commande est suffisamment proche de la commande optimale.
Quand ce n'est pas le cas, on rajoute un terme de régularisation (souvent $\lambda I$) à
la matrice $Q_i^{uu}$ avant de résoudre l'équation~\eqref{quad_solve}.

Pour des problèmes de dimensions plus grandes, on néglige souvent les dérivées secondes
de $f$ dans~\eqref{q_model_recursive} pour réduire le temps de calcul.
En plus, quand les termes du coût à minimiser sont des résidus de moindres carrés
($J(\U)= \tfrac12\lVert \mathbf r(\x,\U)\rVert^2$), on peut également prendre les approximations
$l_i^{xx} = \mathbf r_i^{x\TT{}}\mathbf r_i^x$ et $l_i^{uu} = \mathbf r_i^{u\TT{}}\mathbf
r_i^u$.
Cette approximation est souvent appelée iLQR ou iLQG.\cite{todorov2005generalized}

\subsection{Calcul du gradient}%
\label{sub:gradient_computation}

Pour raisons de vérification, on calcule également le gradient du coût total par rapport
aux commandes pendant le parcours inverse. \\
Ce gradient peut être calculé en introduisant l'état adjoint $p \in \R^{1 \times n}$ du
système, défini par
\begin{align*}
    p_N &= \ell_f^x,\\
    p_i &= p_{i+1} f^x_i + \ell^x_i.
\end{align*}

Soit $d\U$ une petite variation dans la suite de commandes.\\
La variation de la suite d'états correspondante est donnée par
\begin{align*}
    dx_0 &= 0,\\
    dx_{i+1} &= f^x_i dx_i + f^u_i du_i.
\end{align*}
Et la variation du coût total $dJ$ est
\begin{align*}
    dJ &= \sum_{i=0}^{N-1} d\ell_i + d\ell_f,\\
       &= \sum_{i=0}^{N-1} \big(\ell^x_i dx_i + \ell^u_i du_i\big) +
       \ell^x_f dx_N,\\
       &= \sum_{i=0}^{N-1} \big(p_i dx_i - p_{i+1}f^x_i dx_i + \ell^u_i du_i\big) +
       \ell^x_f dx_N,\\
       &= \sum_{i=0}^{N-1} \big(p_i dx_i - p_{i+1}dx_{i+1} + p_{i+1}f^u_i+
       \ell^u_i du_i\big) + \ell^x_f dx_N,\\
       &= p_0 dx_0 + (\ell^x_f - p_N) dx_N + \sum_{i=0}^{N-1}
       (p_{i+1}f^u_i + \ell^u_i)du_i,\\
    dJ &= \sum_{i=0}^{N-1} (p_{i+1}f^u_i + \ell^u_i)du_i.
\end{align*}
Le gradient du coût total par rapport à $u_i$ est alors
\[
    \nabla_{u_i} J = (p_{i+1}f^u_i + \ell_i^u)\T.
\]
Le gradient tout en entier peut être utilisé pour affirmer que notre direction de
recherche est effectivement une direction de descente, étant donné un pas suffisamment
petit, i.e., que $\nabla J\T d\hat\U \leq 0$.\\
Si ce n'est pas le cas, on peut la corriger en inversant la direction des feedforwards ou
en augmentant le paramètre de régularisation.

\subsection{Contraintes sur la commande}%
\label{sub:contraintes_sur_la_commande}

Dans beaucoup de problèmes de commande optimale, la commande est soumises à des
contraintes d'inégalité $b^{-} \leq u \leq b^{+}$, déterminées par les limites physiques
du système.\\
L'algorithme de programmation différentielle dynamique peut être adapté pour gérer ce
type de contraintes.\cite{tassa2014control}
La résolution du système linéaire~\eqref{ff_solve} pour le calcul du feedforward est
remplacée par un problème d'optimisation quadratique:
\begin{equation}
    \label{ff_quadprog}
    k_i = \argmin_k \tfrac12 k\T Q_i^{uu} k + q_i^u k.
\end{equation}
La matrice du feedback est ensuite donnée par
\begin{subequations}\label{fb_quadprog}
    \begin{align}
        K_{i, f} &= -{(Q_i^{uu})}^{-1} Q_{i,f}^{ux},\\
        K_{i, c} &= 0,
    \end{align}
\end{subequations}
où $K_{i,f}$ est la sous-matrice des colonnes de $K$ correspondant aux contraintes non
actives à l'optimum de~\eqref{ff_quadprog}, et $K_{i,c}$ est la sous-matrice
correspondant aux contraintes actives.

On impose également les bornes de la commande durant le parcours direct, en remplaçant
l'équation~\eqref{fwd_pass} par
\[
    \hat u_i = \llbracket u_i + \alpha k_i + K_i (\hat x_i - x_i)
    \rrbracket_{b^{-}}^{b^{+}},\\
\]
où
\begin{equation*}\llbracket u \rrbracket_a^b = \begin{cases}
    b \quad \text{si $u > b$},\\
    a \quad \text{si $u < a$},\\
    u \quad \text{si $a \leq u \leq b$}.
\end{cases}\end{equation*}

Il est important de noter que dans ce cadre de commande avec contraintes, la stratégie de
régularisation précédente ne garantit pas une convergence quadratique, car la valeur
minimal du facteur de régularisation $\lambda$ au voisinage de l'optimum peut ne pas être
nulle.\\
Dans ce cas, on propose de rajouter un deuxième terme qui n'affecte que les coordonnées
correspondant au contraintes actives, ce qui revient à remplacer $Q_i^{uu}$ dans les
équations~\eqref{ff_quadprog} et~\eqref{fb_quadprog} par
\begin{equation*}
    Q_i^{uu} + \lambda_1 I_c + \lambda_2 I_f,
\end{equation*}
où $I_c$ (resp.\ $I_f$) sont des matrices diagonales dont les termes diagonaux sont $1$
pour les indices correspondant aux contraintes actives (resp.\ inactives) et $0$ sinon.\\
Avec ces deux termes de régularisation, il est possible d'avoir une convergence
quadratique, en faisant tendre $\lambda_2$ vers $0$, tout en garantissant que le problème
quadratique reste un problème convexe.
