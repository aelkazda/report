\begin{figure}[htpb]
    \centering
    \includegraphics{plots/invpend.pdf}
    \caption{Pendule inversé sur un chariot}%
    \label{fig:name}
\end{figure}

On applique les méthodes discutées dans ce rapport à un système de pendule inversé sur un
chariot.
En temps continu, ce système est régi par l'équation de la dynamique
\begin{align*}
    \label{eq:inv_pend_dyn}
    \dot x &= v,\\
    \dot \theta &= v_{\theta},\\
    \dot v &=  \frac{u -mlv_\theta^2 \sin\theta + mg\cos\theta \sin\theta}{M +
    m(1-\cos^2\theta)} ,\\
    \dot v_\theta &= \frac{\dot v \cos\theta + g\sin\theta}{l},
\end{align*}
où la commande $u$ représente une force scalaire externe.

On s'intéresse à la possibilité de commander le système en temps discret ($\Delta t =
0.01\mathrm{ s}$).
Dans ce cas, la forme explicite des équations de la dynamique est inconnue, mais elles
peuvent être calculées numériquement en intégrant l'équation différentielle du système
dynamique.\\
On simule le système pour générer un jeu de données.\\
Les états et la commande utilisés pour le construire ${(x_i, \theta_i, v_i,
v_{\theta,i})}_{i\in\{1,\dots, n\}}$ sont i.i.d, distribués uniformément sur
\[[-5,5]\times[-\pi, \pi]\times[-10, 10]\times[-10, 10]\times[-15,15].\]

\section{Estimateur par base de fonctions}%
\label{sec:estimateur_par_base_de_fonctions}

Pour le premier estimateur, la base d'observables est construite à partir de produits
de bases de fonctions univariées.
On a choisit une base trigonométrique pour la variable $\theta$, et des bases
polynomiales pour les autres.\\
Les observables sont alors sous la forme
\begin{align*}
    \phi_{i,2j,k,l,m}(x, \theta, v, v_\theta, u) &= x^i \cdot \cos j \theta \cdot v^k
    \cdot v_\theta^l \cdot u^m,\\
    \phi_{i,2j+1,k,l,m}(x, \theta, v, v_\theta, u) &= x^i \cdot \sin(j+1) \theta \cdot
    v^k \cdot v_\theta^l \cdot u^m.
\end{align*}
Pour en choisir un nombre fini, on fixe un entier $d > 0$ et on choisit tous les
observables $\phi_{i,j,k,l,m}$ tel que $i+j+k+l+m \leq d$.\\
Les coefficients de la base sont déterminés selon~\eqref{lstsq}.

La figure~\ref{fig:lstsq} montre l'erreur de cet estimateur sur un échantillon de test
pour $d=3,5,7,9,11$.\footnote{Pour $d$ pair, l'estimateur obtenu n'est pas amélioré
comparé à celui à $d-1$, à cause des symétries du système.}\\
Expérimentalement, le conditionnement du problème de moindres carrées  à résoudre croît
rapidement avec $d$.\\
Pour $d=11$, le conditionnement est de l'ordre de $4\cdot10^{13}$. Pour $d=13$, il est
de l'ordre de $10^{16}$, ce qui rend le modèle très sensible aux erreurs dans les
données, et limite la précision d'estimation.\\
Il peut être alors intéressant d'implémenter une stratégie de régularisation pour alléger
l'effet des problèmes numériques.

\begin{figure}[H]
    \centering
    \hspace*{-1.5cm}\includegraphics{plots/lstsq.pdf}
    \caption{Erreur de test du modèle de régression par base d'observables en fonction de
    la taille de la population d'entraînement}%
    \label{fig:lstsq}
\end{figure}

\section{Estimateur de régression à noyau}%
\label{sec:kernel_estimator}

Le noyau utilisé pour ce problème est aussi construit comme produit de noyaux univariés.
\[
    K(x, \theta, v, v_\theta, u) = \exp\frac{\cos \theta -
        1}{\sigma_\theta^2}\exp\frac{-\lVert v \rVert^2}{\sigma_v^2}\exp\frac{-\lVert
    v_\theta \rVert^2}{\sigma_{v\theta}^2}\exp\frac{-\lVert u \rVert^2}{\sigma_u^2}.
\]
Ce noyau prend en considération la périodicité de la dynamique par rapport à l'angle,
ainsi que son indépendance de la position $x$ du chariot.\\
$\sigma_\theta,\sigma_v,\sigma{v,\theta},\sigma_u$ ainsi que les coefficients de
régularisation $\lambda_x, \lambda_\theta, \lambda_v, \lambda_{v\theta}$ sont des
hyperparamètres du modèle.

Pour les choisir, on sépare le jeu de données en un échantillon d'entraînement et un
échantillon de validation.
Les hyperparamètres sont ensuite déterminés pour minimiser l'erreur de validation du
l'estimateur qui est entraîné sur l'échantillon de test.\\
Cela peut être implémenté comme problème d'optimisation générique.

La figure~\ref{fig:kernel_err} présente la performance de l'estimateur à noyau sur
l'échantillon de test.
La stagnation du modèle pour des populations plus grandes est dû à des problèmes
numériques, mais ceux là peuvent être mieux contrôlées, en mettant par exemple des
contraintes d'inégalité sur le conditionnement du système dans le problème
d'optimisation, si les méthodes utilisées permettent de le calculer.

\begin{figure}[H]
    \centering
    \hspace*{-1.5cm}\includegraphics{plots/kernel_fig.pdf}
    \caption{Erreur de test du modèle à noyau en fonction de la taille de la population
    d'entraînement}%
    \label{fig:kernel_err}
\end{figure}

\section{Commande optimale}%
\label{sec:commande_optimale}

On calcule l'estimateur à noyau ainsi que ses dérivées pour appliquer l'algorithme de
PDD.\
On commence à l'état initial $\theta=\pi,\ x,v,v_\theta=0$ (position du pendule en bas),
et on construit une fonction coût pour essayer de l'inverser en passant à un état où
$\theta, v,v_\theta=0$ en $5$ secondes ($T = 500$ instants en temps discret).
\[
    J(\U) = 10^{-5}\sum_{i=0}^{T-1} (x_i^2 + v_i^2 + v_{\theta,i}^2 + 0.01 u_i^2) +
    \tfrac12\theta_T^2 + \tfrac12v_T^2+ \tfrac12v_{\theta,T}^2
\]
La PDD minimise cette fonction coût, en utilisant l'estimation donnée de la dynamique.
La commande obtenue est ensuite utilisée pour simuler la trajectoire réelle, calculée
en utilisant la dynamique continue du système.

On compare sur la figure~\ref{fig:traj} les $3$ trajectoires:
\begin{itemize}
    \item Trajectoire estimée par le modèle à noyau,
    \item Trajectoire réelle avec la commande en boucle ouverte,
    \item Trajectoire réelle avec la commande en boucle fermée.
\end{itemize}

Une légère déviation entre la trajectoire estimée et la trajectoire réelle en boucle ouverte
est causée par l'erreur d'estimation du modèle.
Mais le feedback de la commande obtenue par la PDD permet de corriger cette différence
avec une bonne précision.

\begin{figure}[H]
    \centering
    \hspace*{-0.7cm}\includegraphics{plots/one_traj.pdf}
    \caption{Trajectoires suivies par le système, estimée et réelles}%
    \label{fig:traj}
\end{figure}

On présente également les trajectoires de l'angle (en boucle fermée), pour différents
angles (figure~\ref{fig:pos_fig}) et vitesses angulaires (figure~\ref{fig:vel_fig})
initiales.
On arrive à trouver une bonne commande dans la majorité des cas.

\begin{figure}[H]
    \centering
    \hspace*{-0.7cm}\includegraphics{plots/pos_fig.pdf}
    \caption{Trajectoire du système pour différents angles initiaux}%
    \label{fig:pos_fig}
\end{figure}

\begin{figure}[H]
    \centering
    \hspace*{-0.7cm}\includegraphics{plots/vel_fig.pdf}
    \caption{Trajectoire du système pour différentes vitesses angulaires initiales}%
    \label{fig:vel_fig}
\end{figure}
